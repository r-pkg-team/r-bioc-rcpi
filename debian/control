Source: r-bioc-rcpi
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>,
           Steffen Moeller <moeller@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-rcpi
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-rcpi.git
Homepage: https://bioconductor.org/packages/Rcpi/
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-biostrings,
               r-bioc-gosemsim,
               r-cran-curl,
               r-cran-doparallel,
               r-cran-foreach,
               r-cran-httr2,
               r-cran-jsonlite,
               r-cran-rlang,
               pkgconf,
               r-bioc-pwalign
Testsuite: autopkgtest-pkg-r

# pkgconf is a dependency of r-base-dev; adding it here causes dh-update-R
# to keep all following r-* deps (like r-bioc-pwalign) even though they aren't
# listed explicitly in DESCRIPTION

Package: r-bioc-rcpi
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends},
         pkgconf,
         r-bioc-pwalign
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: molecular informatics toolkit for compound-protein interaction
 Rcpi offers a molecular informatics toolkit with a
 comprehensive integration of bioinformatics and
 chemoinformatics tools for drug discovery.
